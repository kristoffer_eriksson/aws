import * as express from "express";
import * as path from 'path';

const app = express();

app.get("/style.css", ( req : any, res : any ) => {
  res.set("Content-Type", "text/css");
  res.sendFile(path.join(__dirname, "../style.css"));
} );

app.get("/bundle.js", ( req : any, res : any ) => {
  res.set("Content-Type", "text/javascript");
  res.sendFile(path.join(__dirname, "../bundle.js"));
} );

app.get("/", ( req : any, res : any ) => {
  res.set("Content-Type", "text/html");
  res.sendFile(path.join(__dirname, "../src/index.html"));
} );

app.listen(8000, () => {
  console.log("App listening on 8000");
});